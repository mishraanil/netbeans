package Model;
// Generated Mar 29, 2017 8:49:13 PM by Hibernate Tools 4.3.1



/**
 * Student generated by hbm2java
 */
public class Student  implements java.io.Serializable {


     private int rollno;
     private String sname;

    public Student() {
    }

    public Student(int rollno, String sname) {
       this.rollno = rollno;
       this.sname = sname;
    }
   
    public int getRollno() {
        return this.rollno;
    }
    
    public void setRollno(int rollno) {
        this.rollno = rollno;
    }
    public String getSname() {
        return this.sname;
    }
    
    public void setSname(String sname) {
        this.sname = sname;
    }




}


