/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ICIT
 */
    public class Connectioncrud {

    Connection conn;

    public Connectioncrud(String username, String database, String password) throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println(" " + "jdbc:mysql://localhost:3306/" + database + ", " + username + "," + password);
            this.conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, username, password);
            System.out.println("Connected Successfully");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("I am from catch block");
        }
    }

    public ResultSet selectRecord(String select_sql) throws SQLException {
        Statement stmt = this.conn.createStatement();//creating statement oject using create satetmentfunction of Connection class
        ResultSet rs = stmt.executeQuery(select_sql);//excecute sql string by using statement oject and store result in ResultSet oject
        return rs;
    }

    public void insertRecord(String insert_sql) throws SQLException {
        Statement stmt = this.conn.createStatement();
        stmt.execute(insert_sql);
    }

    public void updateRecord(String update_sql) throws SQLException {
        Statement stmt = this.conn.createStatement();
        stmt.execute(update_sql);

    }

    public void deleteRecord(String delete_sql) throws SQLException {
        Statement stmt = this.conn.createStatement();
        stmt.execute(delete_sql);
    }
    }

