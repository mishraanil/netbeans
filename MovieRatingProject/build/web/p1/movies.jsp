<%-- 
    Document   : movie
    Created on : Mar 9, 2017, 8:20:18 PM
    Author     : ICIT
--%>

<%@page import="database.Connectioncrud"%>
<%@page import="java.sql.ResultSet"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Connectioncrud r = new Connectioncrud("root", "movies", "");
    String showlist = "SELECT moviename, rating FROM movies";
    //out.println(showlist);
    ResultSet rs = r.selectRecord(showlist);
    String userlogin = request.getParameter("username");
    out.println("Welcome " + userlogin + " to movie rating page");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div>
            <a href="home.jsp">HOME</a>
        </div>
        <div id="header">
            <div style="float: right">
                <a href="logout.jsp">Logout</a>
            </div>
            <div>
                <center>Welcome <%out.println(userlogin);%></center>
            </div>
            <h1 style="margin-left: 10%">Please share the rating for the below listed movies</h1>
            <div style="margin-left: 30%">
                <table border="1">
                    <tr>
                        <td>
                            MovieName
                        </td>
                        <td>
                            Rating
                        </td>
                        <td>
                            Action          
                        </td>
                    </tr>
                    <% while (rs.next()) {%>
                    <tr>
                        <td>
                            <%=rs.getString("moviename")%>
                        </td>
                        <td>
                            <input type="text" name="rating">
                        </td>
                        <td>
                            <a href="set.jsp">Set</a>
                        </td>
                    </tr>
                    <%}%>
                </table>
            </div>
                </div>
    </body>
</html>
