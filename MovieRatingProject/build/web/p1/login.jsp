<%-- 
    Document   : login
    Created on : Mar 9, 2017, 8:19:32 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            #loginpage{
                margin-top: 10%;
                margin-left: 30%;
            }
        </style>
    </head>
    <body>
        <div>
            <a href="home.jsp">HOME</a>
        </div>
        <div id="loginpage">
            <form action="home.jsp">
            <table>
                <tr>
                    <td>
                        Username
                    </td>
                    <td>
                        <input type="text" name ="username" placeholder="username">
                    </td>
                </tr>
                <tr>
                    <td>
                        Password
                    </td>
                    <td>
                        <input type="password" name ="password" placeholder="password">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="LogIn">
                    </td>
                </tr>
            </table>
            </form>    
        </div>
    </body>
</html>
