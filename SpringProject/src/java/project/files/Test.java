package project.files;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/myproject")
public class Test {
    
    @RequestMapping("/userurl")
    public ModelAndView task(){
        ModelAndView mv = new ModelAndView("test");
        return mv;
    }
    @RequestMapping("/myurl")
    public ModelAndView task2(){
        
        ModelAndView mv = new ModelAndView("test2");
        
        String return_value = this.getMessage();
        
        mv.addObject("jspvarname", return_value);
        
        return mv;
    }
    
    public String getMessage(){
        return "My name is Khan I am not terrorist";
    }
}
