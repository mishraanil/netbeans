<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        
        <style>
            
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <tr id="response">
                <td>helloworldpage1</td>
                <td>helloworldpage1</td>
                <td>helloworldpage1</td>
            </tr>
        </table>
        
        <div id="result" style="overflow: none;">
            <div>A</div>
            <div>B</div>
            <div>C</div>
        </div>
        
     
        <button id="call-json-button" onclick="jsoncall()">Call Json</button>
        <button id="call-json-button" onclick="callFromPhp()">Call to Facebook Api</button>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        
        
        function jsoncall() {
            $.ajax({
                url: "http://localhost:8084/AjaxJasonDemoAmit/source.jsp" ,
                method: "GET"
             }).done(function(data){
                 data = $.parseJSON(data);
                 console.log(data);
                 for(i = 0; i < 4; i++) {
                    console.log(data[i].name);
                    console.log(data[i].last_name);
                     $('#result').append("<div><a href='" + data[i].url + "' target='_blank'>" + data[i].name + "</a></div>");
                 }
                 $('#call-json-button').hide();
             });
         }

         function callFromPhp() {
             
             $.ajax({
                 url: "https://feeds.citibikenyc.com/stations/stations.json",
                 method: "GET"
             }).done(function(data){
                 console.log(data);
             });
             
         }
    
        $(document).ready(function(){
            
        })
    </script>
</html>
