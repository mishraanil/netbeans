<%-- 
    Document   : add_user
    Created on : Jan 10, 2017, 9:02:02 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a href="index.html" align="center">Rating Page</a><br>
        <a href="add_moviename">Add MovieName</a>
        <h3 align="center">Add User Details</h3>
        <form>
            <table border="1" align="center">
                <tr>
                    <td>
                        User Name :
                    </td>
                    <td>
                        <input type="text" name="username" placeholder="User Name">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Add">
                        <input type="reset" value="Clear">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
