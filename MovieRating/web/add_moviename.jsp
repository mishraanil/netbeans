<%-- 
    Document   : add_moviename
    Created on : Jan 10, 2017, 8:52:27 PM
    Author     : ICIT
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%

    Connection conn = null;
    try {
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movie_rating", "root", "");
        out.println("Connected Successfully");
    } catch (ClassNotFoundException e) {
        out.println("" + e);
        out.println("I am from catch block");
    }

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a href="index.html" >Rating Page</a><br>
        <a href="add_user.jsp">Add User</a>
        <h3 align="center">Add Movies</h3><br>
        <form action="add_moviename_action.jsp">
            <table border="1" align="center">
                <tr>
                    <td>
                        Movie Name :
                    </td>
                    <td>
                        <input type="text" name="movie_name" placeholder="Movie Name">
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Add">
                        <input type="reset" value="Clear">
                    </td>
                </tr>
            </table>
            
        </form>
    </body>
</html>
