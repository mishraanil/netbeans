package sachin_corejava;

/* Class circle which we gonna use in main class by creating its object with new keyword */     
class Circle {
    int radius; /*datamember*/
    
    void setRadius(int r) { /*functions*/
        this.radius = r;/* storing local variable values into class variables*/
        System.out.println("Value from main " + r);
    }
    
    void showRadius() {
        System.out.println("Value of Circle's radius " + this.radius);
    }
    
    void area() {
        double area = 3.14 * this.radius * this.radius;
        System.out.println("Area of circle is " + area);
    }
}


class Rectangle2 {
    
    int length,breadth; /*declare class rectangle -->data members length and breadth 
                         it stores in heap memory..allocate memory as we create object with new keyword in main..when this new keyword line excute..it will 
    get memory in heap, this will destroy when whole program excute..means when main class close*/
    
 
    
    
    /*All function always run in stack memory except main class(it runs in RAM),once function will be used,it destroy */
    
    void setDimensions (int leng,int bread)  /*here values coming from main..this variables destroy as function close */{
    System.out.println("length and breadth are" +" "+ leng +" " +breadth +" ");/* local function ke  leng  and bread */ 
   
    /*now leng and bread which we received from main ..we have to store it in somewhere..so we are storing it in class variable (lenght and breadth)-->
    this class variable destroy when main class close*/
    
    /*to access class variable or class function within the fuctions...we have to use this keyword
    this keyword is used..it means it is linked with object*/
    
    this.length=leng; /* storing local variable values into class variables*/
    this.breadth=bread;/* storing local variable values into class variables*/
      System.out.println("Value from main " + length + " " + breadth + " ");
    
            }


   void area () {
            int area = (this.length* this.breadth);
            System.out.println("area of rectangle" + " " + area+ "");
       
    }

     void perimeter () { 
          int perimeter = 2* (length + breadth);
          System.out.println( " perimeter of rectangle" + "" + perimeter+ "");
    }
}

class Cube {
    int lenght,breadth,height;
    
    void setDimensions(int leng,int bread,int heig) { 
       
        
        this.lenght = leng;
        this.breadth = bread;
        this.height = heig;
        
         System.out.println("lenght and breadth from main are" + leng + " " + bread + "" + heig + " ");
         
    }
    
    void volume () {
    
   double vol = (this.lenght * this.breadth * this.height);
   System.out.println("volume of cube = " + vol );
   
    }
}

class Worker {
    
    double wages;
    int wdays;
   
    void setdata(int days,int wages)
    {
        this.wdays=days;
        this.wages=wages;
        System.out.println("number of working days=" + days );
         System.out.println("wages per day=" +wages);
    }   
    
    void payment (){
       
        double pay=this.wdays * this.wages;
        System.out.println(" net payment = " +pay);
        
    } 
}

public class Sachin_corejava {
    
    
    /*main fun runs in RAM (primary memory) */
    public static void main(String[] args) {
        Circle c = new Circle();
        Circle d = new Circle();
        Rectangle2 r = new Rectangle2();
        Cube i = new Cube ();
        Worker w = new  Worker ();
        
       
        c.setRadius(5);
        c.showRadius();
        c.area();
       
        d.setRadius(7);
        d.showRadius();
        d.area();
        
        r.setDimensions (4,5);
        r.area();
        r.perimeter();
       
       i.setDimensions(5, 4, 8);
       i.volume();
       
       w.setdata(20, 500);
       w.payment();
       
       Rectangle e = new Rectangle ();
       e.setLength(5);
       e.setBreadth(8);
       
       e.area();
       Rectangle f =new Rectangle ();
       f.area();
       
       Rectangle h =new Rectangle (5,5);
       h.area();
       
    }
}

