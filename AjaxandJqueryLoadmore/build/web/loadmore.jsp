<%-- 
    Document   : loadmore
    Created on : Nov 9, 2016, 7:17:28 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        
        <style>
            
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <tr id="response">
                <td>helloworldpage1</td>
                <td>helloworldpage1</td>
                <td>helloworldpage1</td>
            </tr>
        </table>
        
        <div id="result" style="overflow: none;">
            <div>A</div>
            <div>B</div>
            <div>C</div>
        </div>
        
        <button onclick="ajaxcall()">Load More</button>
        <button id="call-json-button" onclick="jsoncall()">Call Json</button>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        function ajaxcall(){
            
            $.ajax({
               url: "http://localhost:8084/AjaxandJqueryLoadmore/loadmore2.jsp" ,
               method: "GET"
            }).done(function(data){
                console.log(data);
                $('#response').append(data);
            });
            
        }
        
        function jsoncall() {
            $.ajax({
                url: "http://localhost:8084/AjaxandJqueryLoadmore/json_response.jsp" ,
                method: "GET"
             }).done(function(data){
                 data = $.parseJSON(data);
                 console.log(data);
                 for(i = 0; i < 4; i++) {
                    console.log(data[i].name);
                    console.log(data[i].last_name);
                    //$('#response').append("<td>" + data[i].name + "</td>")
                    //$('#response').append("<td>" + data[i].name + "</td>")
                    $('#result').append("<div>" + data[i].name + "</div>");
                 }
                 $('#call-json-button').hide();
             });
         }
     
        $(document).ready(function(){
            
        })
    </script>
</html>
