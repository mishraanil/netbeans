<%-- 
    Document   : read
    Created on : Feb 25, 2017, 7:53:28 PM
    Author     : ICIT
--%>


<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
Connection conn = null;
String database = "icit";
String username = "root";
String password = "";


try {
    Class.forName("com.mysql.jdbc.Driver");
    System.out.println(" " + "jdbc:mysql://localhost:3306/" + database + ", " + username + "," + password);
    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, username, password);
    System.out.println("Connected Successfully");
} catch (ClassNotFoundException | SQLException e) {
    System.out.println("test..................." + e);
    System.out.println("I am from catch block");
}


int page_number = 1;
if(request.getParameter("page_number") != null) {
    page_number = Integer.parseInt(request.getParameter("page_number"));
}

%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>List of Customers</h1>
        
        <%
            int starting_from = 0;
            
            if(page_number == 1) {
                starting_from = 0;
            } else {
                starting_from = (page_number-1) * 5;
            }
        %>
        
        <table border="1">
        <%
            Statement stmt = conn.createStatement();//creating statement oject using create satetmentfunction of Connection class
            ResultSet rs = stmt.executeQuery("SELECT customerNumber, contactLastName, contactFirstName, phone FROM customers LIMIT " + starting_from + ",5");//excecute sql string by using statement oject and store result in ResultSet oject
        %>    
    
        <tr>
            <td>Customer Id </td>
            <td>Customer LastName </td>
            <td>Customer FirstName </td>
            <td>Customer PhoneNumber </td>
        </tr>
    <%
    while (rs.next()){%>
          <tr>
              <td><%
                    out.println(rs.getString("customerNumber") + "<br/>");
                %>
              </td>
         
          
              <td><%
                    out.println(rs.getString("contactLastName") + "<br/>");
                %>
              </td>
          
              <td><%
                    out.println(rs.getString("contactFirstName") + "<br/>");
                %>
              </td>
          
              <td><%
                    out.println(rs.getString("phone") + "<br/>");
                %>
              </td>
          </tr>
        <% } %>
        </table>
        
        <div>
            Page Number 
            <%
                for(int i = 1; i <= 125/5; i++) {
            %>
            <a href="http://localhost:8084/MovieRatingPage/crud/pagepractical.jsp?page_number=<%=i%>">
                <%=i%></a>&nbsp;
            <% } %>
        </div>
        
    </body>
</html>
