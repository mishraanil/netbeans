<%-- 
    Document   : testloginpage
    Created on : Mar 3, 2017, 9:20:40 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <style>
            #login-form {
                color: blue;
                width: 15%;
                margin: 0 auto;
                margin-top: 200px;
            }
            
            #menu {
                overflow: none;
            }
            #home {
                float: left;
            }
            
            #logout {
                float: right;
            }
            
        </style>
        
    </head>
    <body>
        <div id="menu">
            <div id="home">Home</div>
            <div id="logout">Logout</div>
        </div>
        <div style="clear: both;"></div>
        <div id="login-form">
            <form>
                <table>
                    <tr>
                        <td>Username</td>
                        <td><input type="text" name="username"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
