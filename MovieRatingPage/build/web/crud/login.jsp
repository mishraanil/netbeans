<%-- 
    Document   : login
    Created on : Mar 1, 2017, 8:44:16 PM
    Author     : ICIT
--%>

<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.Enumeration"%>
<%
HttpSession sess = request.getSession();

if(sess.getAttribute("sess_username") !=null){
    response.sendRedirect("home.jsp");
}

//out.println(session.getAttribute("email"));
//out.println(session.getAttribute("password"));
//out.println(session.getAttribute("phno"));

out.println("<br/>");
Enumeration enumy = session.getAttributeNames();

while (enumy.hasMoreElements()) {
    out.println(session.getAttribute((String)enumy.nextElement()) + "<br/>");
    
}

ArrayList list = new ArrayList();
list.add("Monday");
list.add("Tuesday");
list.add("Wednesday");
pageContext.setAttribute("weekdays", list);

%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <h1>Login</h1>
        <form action="home.jsp" method="POST">
            Username <input type="text" name="username"/><br>
            Password <input type="password" name="password"/><br>
            <input type="submit" value="Login">
        </form>
        
        <c:forEach var="mynm" items="${sessionScope}">

            ${mynm.key} = ${mynm.value} <br/>

        </c:forEach>
        
        <c:forEach items="${weekdays}" var="mylocalvar">
            ${mylocalvar}
        </c:forEach>
    </body>
</html>
