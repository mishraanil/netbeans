<%-- 
    Document   : logout
    Created on : Mar 1, 2017, 9:09:00 PM
    Author     : ICIT
--%>

<%
HttpSession sess = request.getSession();
sess.invalidate(); // this will terminate the session and destroy all variables stored in session pool
%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
</head>
<body>
<%@include file="header.jsp" %>
<h1>Logout</h1>
</body>
</html>