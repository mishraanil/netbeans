<%-- 
    Document   : jsexcercise
    Created on : Feb 8, 2017, 8:31:48 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            /*
            input[type=button]{
                cursor: pointer;
            }*/
            .button {
                background-color: red;
                color: #ffffff;
            }
            
            .button:hover {
                background-color: #ffffff;
                color: red;
            }
            
            #clickme {
                cursor: pointer;
            }
            
            #hitme {
                cursor: not-allowed;
            }
            
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <input type="text" name="radius" id="radius" value="5"/>
        <input id="clickme" class="button" type="button" value="Click Me" onclick="areac();"/>
        <input id="hitme" class="button" type="button" value="Hit Me" onclick="dusrafun();"/>
        
        <div id="result">
            
        </div>
        
    </body>
    
    
    <script>
        
        function areac(){
               
            var radius = document.getElementById("radius").value;
            var r = radius;
            var diameter = radius * 2;
            var circumference = 2 * 3.14 * radius;
            var area = 3.14 * radius * radius;
            
            console.log("Radius of circle")
            console.log(r);
            console.log("Diameter of circle")
            console.log(diameter);    
            console.log("Circumference of circle")
            console.log(circumference);
            console.log("Area of circle")
            console.log(area);
            
            var result = document.getElementById("result");
            result.innerHTML = "Hello I am from JS to HTML<br/>";
            result.innerHTML = result.innerHTML + "Radius of Circle = " + r + "<br/>";
            result.innerHTML = result.innerHTML + "Diameter of Circle = " + diameter + "<br/>";
            result.innerHTML = result.innerHTML + "Circumference of Circle = " + circumference + "<br/>";
            result.innerHTML = result.innerHTML + "Area of Circle = " + area;
            
        }
        
        
        
    </script>
    
</html>
