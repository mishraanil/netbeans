<%-- 
    Document   : home
    Created on : Mar 3, 2017, 8:14:30 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
HttpSession sess = request.getSession();

if((request.getParameter("username") == null || request.getParameter("username") == "") && sess.getAttribute("sessname")== null) {
    response.sendRedirect("testloginpage.jsp");
}

if(sess.getAttribute("sessname")== null && request.getParameter("username") != ""){
    sess.setAttribute("sessname", request.getParameter("username"));
}
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            #menu{
                float: none;
            }
            #home{
                float: left;
            }
            #logout{
                float: right;
            }
        
        </style>
    </head>
    <body>
        <div id="menu">
            <div id="home">
                <a href="home.jsp">
                    Home
                </a>
            </div> 
            <div id="logout">
                <a href="logout.jsp">Logout</a></div> 
        </div>
        <div style="clear: both;"></div>
    <center><h1>Welcome To my Site</h1>
        <h3>
            <%
                out.println(sess.getAttribute("sessname"));
            %>
        </h3>
    </center>
    </body>
</html>
