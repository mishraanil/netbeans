<%-- 
    Document   : movie.jsp
    Created on : Jan 30, 2017, 8:17:36 PM
    Author     : ICIT
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="database.RatingMoviesSqlCurddatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            h1{
                background: silver;
            }
        </style>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <h2>Movie page</h2>
        <center>
        <%
            RatingMoviesSqlCurddatabase r = new RatingMoviesSqlCurddatabase("root", "", "rating_movie");
            String moviename = request.getParameter("moviename");
            String showlist = "SELECT * FROM movie ORDER BY updated_date DESC LIMIT 5";
            ResultSet rs = r.selectRecord(showlist);
        %>
        
        <table border="1">
            <tr>
                <td>
                    Movie Name
                </td>
                <td colspan="2">
                    Action
                </td>
            </tr>
            <% while (rs.next()){%>
            <tr>
                <td>
                    <%=rs.getString("moviename")%>
                </td>
                <td>
                    <a href="edit_movie.jsp?id=<%=rs.getString("id")%>">
                        Edit
                    </a>
                </td>
                <td>
                    <a href="delete_movie.jsp?id=<%=rs.getString("id")%>">
                        Delete
                    </a>
                </td>
            </tr>
            
            
            <%}%>
        </table>
        </center>
    </body>
</html>
