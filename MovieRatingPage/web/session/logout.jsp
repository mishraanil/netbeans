<%-- 
    Document   : logout
    Created on : Mar 3, 2017, 8:14:06 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
HttpSession sess = request.getSession();
sess.invalidate();
response.sendRedirect("testloginpage.jsp");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file = "header.jsp" %> <br>
        Logout Page
    </body>
</html>
