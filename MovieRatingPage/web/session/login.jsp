<%-- 
    Document   : login
    Created on : Mar 3, 2017, 8:13:52 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
HttpSession sess = request.getSession();
if(sess.getAttribute("sessname") != null){
    response.sendRedirect("home.jsp");
}
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file = "header.jsp" %>
        <br>
        <br>
            Login Page
        <br>
        <br>
        <form action="home.jsp" > 
            <!--method="post"-->
            <table border="1">
                <tr>
                    <td>
                        Username 
                    </td>
                    <td>
                        <input type="text" name="username" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Password 
                    </td>
                    <td>
                        <input type="password" name="password"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Login" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
