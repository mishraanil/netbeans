<%-- 
    Document   : header
    Created on : Nov 12, 2016, 6:15:05 PM
    Author     : ICIT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="<c:url value="/amit/css/default.css"/>" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div>
            <ul>
                <li><a href="javascript:void(0);">Home</a></li>
                <li><a href="<c:url value="/service.htm" />">Service</a></li>
                <li><a href="<c:url value="/about_us.htm" />">About Us</a></li>
            </ul>
        </div>
        <div style="clear: both;"></div>
    