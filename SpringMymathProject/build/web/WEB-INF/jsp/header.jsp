<style>
    div {
/*        border: solid 1px black;*/
    }
    
    #menus {
        float:left;
        margin: 20px 0px 30px 20px;
    }
    
    #greetings {
        float: right;
        margin: 20px 20px 30px 0px;
    }
    
    .clear {
        clear: both;
    }
    
    .main-content {
/*        border: solid 1px black;*/
        width: 30%;
        margin: 0 auto;
    }
</style>
<div>
    <div id="menus">
        <a href="${site_url}/mymath/add_form.htm">Addition</a>
        <a href="${site_url}/mymath/sub.htm">Subtraction</a>
        <a href="${site_url}/mymath/divide.htm">Division</a>
        <a href="${site_url}/mymath/multiply.htm">Multiplication</a>
    </div>
    <div id="greetings">
        Hello <%=session.getAttribute("name")%>
    </div>
</div>
    <div class="clear"></div>
