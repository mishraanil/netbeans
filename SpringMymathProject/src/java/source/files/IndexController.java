/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source.files;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ICIT
 */
@Controller
public class IndexController {
    
    @RequestMapping("/index.htm")
    public ModelAndView welcomePage(HttpSession sess){
        
        ModelAndView mv = new ModelAndView("index");
        sess.setAttribute("name", "Anil Mishra");
        mv.addObject("site_url", "http://localhost:8084/SpringMymathProject");
        return mv;
    }
    
}
