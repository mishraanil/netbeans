package source.files;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/mymath")
public class Mymath {

    public String site_url;
    
    public Mymath() {
        this.setSessionValue();
        this.site_url = "http://localhost:8084/SpringMymathProject";
    }
    
    public void setSessionValue() {
    }
    
    @RequestMapping("/add") // @RequestMapping("url_name.htm")
    public ModelAndView add(@RequestParam("x") int localx ,@RequestParam("y") int localy ){
    
    ModelAndView mv = new ModelAndView("add"); // ModelAndView("jsp page name come here");
   // mv.addObject("jspx", localx);
    mv.addObject("z",this.addition(localx,localy));
    return mv;
    
    }
    
    @RequestMapping("/header")
    public ModelAndView header(){
        ModelAndView mv = new ModelAndView("header");
        
        //mv.addObject("z", "Subtraction is " + z);
        return mv;
    }
    
    @RequestMapping("/subtraction")
    public ModelAndView sub(){
        ModelAndView mv = new ModelAndView("subtract");
        int x=3;
        int y=10;
        int z = y-x;
        mv.addObject("z", "Subtraction is " + z);
        return mv;
    }
    
    @RequestMapping("/divide")
    public ModelAndView divide(){
        ModelAndView mv = new ModelAndView("divide");
        int x=11;
        int y=4;
        int z=x-y;
        mv.addObject("z", "Division is " + z);
        return mv;
    }
    
    @RequestMapping("/mul")
    public ModelAndView multiply(){
        ModelAndView mv = new ModelAndView("multiply");
        int x=5;
        int y=5;
        int z=x*y;
    
        mv.addObject("z","Multiplication is " + z);
        return mv;
    }
    
    @RequestMapping("/add_form")
    public ModelAndView addForm(){
        
        ModelAndView mv = new ModelAndView("addition_form");
        mv.addObject("site_url", this.site_url);
        return mv;
    }
    
    public int addition(int x, int y){
    
    //int x=1;
    //int y=2;
    int z = x+y;
    return z; 
    
    }
}
