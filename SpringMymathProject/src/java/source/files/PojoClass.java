/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source.files;

/**
 *
 * @author ICIT
 */
public class PojoClass {
    String name;
    int age;
    String division;

    public PojoClass(String name, int age, String division) {
        this.name = name;
        this.age = age;
        this.division = division;
    }

    public PojoClass() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
    
    
}
