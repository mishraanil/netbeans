/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import model.Userdata;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ICIT
 */
public class UserDataDAO {
   
    
    Transaction t;
    int x;
    
    public int addData (Userdata myuserdata){
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        
        Session session = sf.openSession();
        
        t = session.beginTransaction();
        session.save(myuserdata);
        
        t.commit();
       
      return 1;
    }
}
