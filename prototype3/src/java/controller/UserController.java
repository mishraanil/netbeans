/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ICIT
 */
@Controller
@RequestMapping("/main")
public class UserController {
    
    @RequestMapping("/display")
    public ModelAndView showMethod(){
        return new ModelAndView("display");
    }
    
    @RequestMapping(value = "/ajaxcall", method = RequestMethod.POST)
    public @ResponseBody String ajaxMethod(@RequestParam("email") String email_id){
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        
        Session session = sf.openSession();
        
        //Transaction t = session.beginTransaction();
        
       List mylist = session.createQuery("From Userdata where email='"+ email_id +"'").list();
       // session.save(myuserdata);
        
      //  t.commit();
       if(mylist.size() > 0) {
           return "true";
       } else {
            return "false";
       }
       
    }
    
}
