<%-- 
    Document   : display
    Created on : Nov 25, 2016, 8:35:20 PM
    Author     : ICIT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="">
        <input type="button" onclick="validCall();" value="Click Me"/>
       
        <input type="email" id="email"></input>
        <div id="displayme">
        </div>
        </form>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script>
        function validCall(){
            $.ajax({
                url: "<c:url value="ajaxcall.htm"/>",
                method: "post",
                data: {email : $('#email').val()},
                success: function(data){
                    console.log("email is" + data);
                    if(data == "true") {
                        $("#displayme").html("&#x2718;");
                    } else {
                        $("#displayme").html("&#10004;");
                        
                    }
                }
            });
        }
        </script>
</html>
