package jdbcdemo;
import java.sql.*;

public class JDBCDemo {

    public static void main(String[] args) throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost/demo", "root", "");
        Statement st = con.createStatement();
        String query = "select * from student where rollno = 2";
        ResultSet rs = st.executeQuery(query);
        rs.next();
        String rolle = rs.getString(1);
        String namme = rs.getString(2);
        System.out.println("Roll no of " + namme + " is " + rolle );           
    }
    
    
}
