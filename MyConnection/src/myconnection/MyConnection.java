package myconnection;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MyConnection {

    public static void main(String[] args) throws SQLException {

        MySqlCrud dbc = new MySqlCrud("root", "", "icit");

        ResultSet rs = dbc.selectRecord("SELECT * FROM students");
        while (rs.next()) {//travers through each record of Resultset
            System.out.println("" + rs.getString("name") + " " + rs.getString("age") + " " + rs.getString("city"));
            //System.out.println("" + rs.getString("age"));
        }
        
        //int usql = dbc.updateRecord("UPDATE students SET name = 'Vijay Chauhan' WHERE name = 'Vijay' ");
        
        int dsql = dbc.deleteRecord("DELETE FROM students WHERE name = 'Vijay Chauhan' ");
    }

}
