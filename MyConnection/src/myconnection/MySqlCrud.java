package myconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlCrud {

    Connection conn;

    public MySqlCrud(String username, String password, String database) {
        try {//trying to make connection with mysql database if any thing goes wrong it will throw exception and catch block will catch that exception.
            Class.forName("com.mysql.jdbc.Driver");//loading mysql jdbc driver where jdbc = java database connection
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, username, password);//connection object for database icit using connection url,username and password
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("" + e);
            System.out.println("I am from catch block");
        }
    }

    public ResultSet selectRecord(String select_sql) throws SQLException {

        Statement stmt = this.conn.createStatement();//creating statement oject using create satetmentfunction of Connection class
        ResultSet rs = stmt.executeQuery(select_sql);//excecute sql string by using statement oject and store result in ResultSet oject
        return rs;
    }

    public  int updateRecord(String update_sql) throws SQLException{
        
        Statement stmt = this.conn.createStatement();//creating statement oject using create satetmentfunction of Connection class
        int usql = stmt.executeUpdate(update_sql);
    return usql;
    }
    
    public  int deleteRecord(String update_sql) throws SQLException{
        
        Statement stmt = this.conn.createStatement();//creating statement oject using create satetmentfunction of Connection class
        int dsql = stmt.executeUpdate(update_sql);
    return dsql;
    
    }    
}
