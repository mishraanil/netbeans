package model;
// Generated Nov 29, 2016 8:39:17 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Userdata generated by hbm2java
 */
public class Userdata  implements java.io.Serializable {


     private Long id;
     private String fname;
     private String lname;
     private String phone;
     private String email;
     private String password;
     private Date newdate;
     private Date updatedate;

    public Userdata() {
    }

	
    public Userdata(String fname, String lname, String email, String password, Date newdate) {
        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.password = password;
        this.newdate = newdate;
    }
    public Userdata(String fname, String lname, String phone, String email, String password, Date newdate, Date updatedate) {
       this.fname = fname;
       this.lname = lname;
       this.phone = phone;
       this.email = email;
       this.password = password;
       this.newdate = newdate;
       this.updatedate = updatedate;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getFname() {
        return this.fname;
    }
    
    public void setFname(String fname) {
        this.fname = fname;
    }
    public String getLname() {
        return this.lname;
    }
    
    public void setLname(String lname) {
        this.lname = lname;
    }
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public Date getNewdate() {
        return this.newdate;
    }
    
    public void setNewdate(Date newdate) {
        this.newdate = newdate;
    }
    public Date getUpdatedate() {
        return this.updatedate;
    }
    
    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }




}


