package Controller;

import java.util.List;
import javax.servlet.http.HttpSession;
import model.Userdata;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/main")

public class MainController {
    @RequestMapping("/index")
    public ModelAndView maincontrol(HttpSession session){
        if(session.getAttribute("email")!= null)
        {
            return new ModelAndView("newjsp");
        }
        else{
             return new ModelAndView("index");
        }
       
    }
    
}
