
package anil.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Test {
    
    @RequestMapping("/mytest")
    public ModelAndView test() {
        return new ModelAndView("test");
    }
    
    @RequestMapping("/yourtest")
    public ModelAndView urtest() {
        return new ModelAndView("yourtest");
    }
    
}
