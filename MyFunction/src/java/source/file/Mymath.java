package source.file;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller

@RequestMapping("/mymath")
public class Mymath {
    
    int x;
    int y;
    
    @RequestMapping("/header")
    public ModelAndView header(){
        ModelAndView mv = new ModelAndView("header");
        
        return mv;
    }
    @RequestMapping("/addition")
    public ModelAndView add(){
        ModelAndView mv = new ModelAndView("add");
        return mv;
    }
    @RequestMapping("/additionaction")
    public ModelAndView addaction(@RequestParam("x") int x , @RequestParam("y") int y){
        this.x = x;
        this.y = y;
        ModelAndView model = new ModelAndView("addaction");
        
        model.addObject("x", this.x);
        model.addObject("y", this.y);
        
        int z = x+y;
        
        model.addObject( "z", z);
        
        return model;
    }
    
    public int additionAction() {
        return this.x + this.y;
    }
}
