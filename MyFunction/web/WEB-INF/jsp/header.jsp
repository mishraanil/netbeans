<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="<c:url value="/resources/css/default.css"/>" rel="stylesheet" type="text/css"/>
        <script src="<c:url value="/resources/js/default.js"/>"></script>
        <style>
            #user{
                float: left;
            }
            #logout{
                float: right;
            }
            #menu {
                text-align: center;
            }
            
            #add-form {
/*                border: solid 1px black; */
                text-align: center;
                width: 260px; 
                height:200px; 
                margin: 50px auto;
            }
        </style>    
    </head>
    <body>
        <div>
            <div id="user">
                Hello User
            </div>
            <div id="logout">
                Logout
            </div>
        </div><br>
        
        <div>
            <center><img width="200px" height="100px" src="<c:url value="/resources/images/logo.gif"/>"/></center>
        </div>
        
        <div id="menu">
            <a href ="http://localhost:8084/MyFunction/mymath/addition.htm">Addition</a>
            <a href ="addaction.jsp">Subtraction</a>
            <a href ="addaction.jsp">Division</a>
            <a href ="addaction.jsp">Multiplication</a>
        </div>
        