public class YesBank implements RBIbank{

int balance;
int account_no;

    public YesBank(int acc_no, int balance) {
        this.account_no = acc_no;
        this.balance = balance;
    }
        
    

    public void withDraw(int amount) {
        //System.out.println("I am from yesbank withdraw method");
       this.balance = this.balance - amount; 
    }

    public void yesBank(){
        System.out.println("This function is from yes bank");
    
    }

    @Override
    public void mobileBanking() {
        
    }

    @Override
    public int deposit(int amount) {
        this.balance = this.balance + amount;
        return this.balance;
    }
    

    public void showBalance(){
        System.out.println("===============================");
        System.out.println("Bank Account No " + this.account_no);
        
        System.out.println("The Current balance is " + this.balance);
        
    }
    
   
    
}
