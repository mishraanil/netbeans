/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author ICIT
 */
public class Bank {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        YesBank yb1 = new YesBank(1345678,500);
        YesBank yb2 = new YesBank(23456,1000);
        
        yb1.showBalance();
        
        yb2.showBalance();
        System.out.println("===============================");
        System.out.println("Total Balance is " + yb1.deposit(1000));
        System.out.println("After deposit");
        yb1.showBalance();
        yb1.withDraw(200);
        yb2.withDraw(500);
        System.out.println("After Withdraw");
        yb1.showBalance();
        yb2.showBalance();
                
    }
    
}
