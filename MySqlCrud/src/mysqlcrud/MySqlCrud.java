package mysqlcrud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlCrud {

    public static void main(String[] args) throws SQLException {
        try {//trying to make connection with mysql database if any thing goes wrong it will throw exception and catch block will caught that exception
            Class.forName("com.mysql.jdbc.Driver");//loading mysql jdbc driver where jdbc = java database connection
            
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/icit", "root", "");//connection object for database icit using connection url,username and password

            Statement stmt = conn.createStatement();//creating statement oject using create satetmentfunction of Connection class
            //String select_sql = "SELECT * FROM students";//create sql string
            String insert_sql = "INSERT INTO students (id,name,age,city,mobile_number) VALUES (NULL,'Vijay',25,'Kerala','123456789')";
            
            //ResultSet rs = stmt.executeQuery(insert_sql);//excecute sql string by using statement oject and store result in ResultSet oject
            
            stmt.execute(insert_sql);
          
            /*while (rs.next()) {//travers through each record of Resultset
                System.out.println("" + rs.getString("name")+ " " + rs.getString("age")+" " + rs.getString("city"));
                //System.out.println("" + rs.getString("age"));
            }
*/
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("" + e);
            System.out.println("I am from catch block");
        }

    }

}
