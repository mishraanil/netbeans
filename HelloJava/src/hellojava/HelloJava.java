
package hellojava;

public class HelloJava {

    public static void main(String[] args) {
        
        int number1 = 1234;
        
        System.out.println("Given number = " + number1);
        
        int first_digit = number1/1000;
        int second_digit = (number1/100) % 10;
        int third_digit = (number1/10) % 10;
        int last_digit = number1%10;
        
        System.out.println("first digit = " + first_digit);
        System.out.println("second digit = " + second_digit);
        System.out.println("third digit = " + third_digit);
        System.out.println("fourth digit = " + last_digit);
        
    }
    
}
