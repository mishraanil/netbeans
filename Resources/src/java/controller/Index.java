package controller;

import java.util.Iterator;
import java.util.List;
import model.Customers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Index {
    
    @RequestMapping("index")
    public ModelAndView welcomePage() {
        ModelAndView mv = new ModelAndView("index");
        
        Configuration cnf = new Configuration();         
        
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());                 
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());         
        Session session = sf.openSession();                 
        String hql = "FROM Customers";                 
        
        List<Customers> list = session.createQuery(hql).list();                 
        
//        for(Iterator iterator = list.iterator(); iterator.hasNext();) {             
//            Customers e = (Customers)iterator.next();
//        }                 
        
        mv.addObject("result_set", list);
        
        return mv;
    }
    
    @RequestMapping("welcome")
    public ModelAndView testPage() {
        ModelAndView mv = new ModelAndView("test");
        return mv;
    }
    
}
