/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import model.Userdata;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ICIT
 */

public class UserDAO {
    
    public List getData(){
         Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        Session session = sf.openSession();
        
        String hql = "FROM Userdata";
        
        List list = session.createQuery(hql).list();
        
        for(Iterator iterator = list.iterator(); iterator.hasNext();) {
            Userdata e = (Userdata)iterator.next();
            
            System.out.println(e.getFname());
        }
        
        return list;
    }
    
    public int addDATA (Userdata myuserdata){
        Configuration cnf = new Configuration();
        cnf.configure("hibernate.cfg.xml");
        
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder().applySettings(cnf.getProperties());
        
        SessionFactory sf = cnf.buildSessionFactory(ssrb.build());
        Session session = sf.openSession();
        
        Transaction t = session.beginTransaction();
        session.save(myuserdata);
        
        t.commit();
        return 1;
    }
}
