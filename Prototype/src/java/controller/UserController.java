/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Date;
import model.DAO.UserDAO;
import model.Userdata;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ICIT
 */
@Controller
@RequestMapping("/testuser")

public class UserController {
    @RequestMapping("/showalluser")
    public ModelAndView showUserData(@RequestParam("fname") String fname){
        UserDAO obj = new UserDAO();
        Userdata obj1 = new Userdata(); 
        obj1.setId(Long.MIN_VALUE);
        obj1.setFname(fname);
        obj1.setLname("karkera");
        obj1.setEmail("amit@yahoo.com");
        obj1.setPhone("123456789");
        obj1.setPassword("root1");
        obj1.setAddDate(new Date());
        obj1.setUpdatedDate(new Date());
        obj.addDATA(obj1);
        return new ModelAndView("testHibernate");
        
    }
    
    
}
