/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ICIT
 */
@Controller
@RequestMapping("/main")
public class Loadmore {
    
    @RequestMapping("/loadmore")
    public ModelAndView connect(){
        return new ModelAndView("loadmore");
    }
    
    @RequestMapping(value = "/ajaxloadmore", method = RequestMethod.GET)
    public @ResponseBody String fetchjason(){
        return "[{\"name\": \"John\" }, { \"name\": \"Shailesh\" }, { \"name\": \"Amit\"}]";
    }
    
}
