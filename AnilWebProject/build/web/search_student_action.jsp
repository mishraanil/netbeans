<%-- 
    Document   : search_student_action
    Created on : Jan 3, 2017, 8:40:51 PM
    Author     : ICIT
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.MysqlCrudDb1"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    int totalrecord = 0;
    MysqlCrudDb1 p = new MysqlCrudDb1("root", "", "icit");
    String n_ame = request.getParameter("name");
    String city = request.getParameter("city");
    String age = request.getParameter("age");

    String where_str = " 1=1 ";

    if (n_ame != null && !n_ame.equals("")) {
        where_str = where_str + " AND name = '" + n_ame + "'";
    }
    
    if(city != null && !city.equals("")) {
        where_str = where_str + " AND city = '" + city + "'";
    }
    
    if(age != null && !age.equals("")){
       where_str = where_str + "AND age = '" + age + "'"; 
    }

    out.println(where_str);
    String sql = "SELECT * FROM students WHERE " + where_str;
    ResultSet rs        = p.selectRecord(sql);
    
    while (rs.next()) {
        totalrecord++;
    }
    rs.beforeFirst();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            if(totalrecord > 1) {
        %>
        <table border="1">
            <tr>
                <td colspan="1">
                    Name </td>
                <td colspan="1">
                    Age</td>
                <td colspan="1">
                    City</td>
                <td>
                    Action
                </td>
            </tr>
            <% while (rs.next()) {%>

            <tr>
                <td colspan="1">
                    <%=rs.getString("name")%> 
                </td>
                <td colspan="1">
                    <%=rs.getString("age")%>
                </td> 
                <td colspan="1">
                    <%=rs.getString("city")%>
                </td>
                <td>
                    <a href="edit_student_data.jsp?id=<%=rs.getString("id")%>">
                        edit
                    </a>
                        &nbsp;
                    <a href="delete_student_action.jsp?student_id=<%=rs.getString("id")%>" onclick="return confirm('Are you sure you want to delte this record?')">
                        delete
                    </a>   
                </td>
            </tr>

            <% }%>            
        </table>
        <% } else { %>
            <table border="1">
                <% while (rs.next()) {%>
                <tr>
                    <td colspan="1">
                        Name 
                    </td>
                    <td colspan="1">
                        <%=rs.getString("name")%>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Age
                    </td>
                    <td colspan="1">
                        <%=rs.getString("age")%>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        City
                    </td>
                    <td colspan="1">
                        <%=rs.getString("city")%>
                    </td>
                </tr>
                <% }%>            
            </table>
        <% } %>
    </body>

</html>
