<%-- 
    Document   : studentdata
    Created on : Dec 28, 2016, 8:19:44 PM
    Author     : ICIT
--%>
<%@page import="javax.swing.JOptionPane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.Student" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            Student s = new Student(request.getParameter("name"), 33);

            for (int i = 0; i < 10; i++) {
                    out.println(i);
                }

        %>
        <h1>
            <%=s.getName()%>
            <%=s.getAge()%>    
            
            <%
                s.setName("Shailesh Sonarae");
                s.setAge(25);
            %>
            
            <%
                int num = Integer.parseInt(request.getParameter("num"));
                out.println(num * num);
            %>
        </h1>
    </body>
</html>
