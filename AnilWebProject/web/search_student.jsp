<%-- 
    Document   : project1
    Created on : Jan 3, 2017, 8:05:52 PM
    Author     : ICIT
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.MysqlCrudDb1"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a href="index.html">Home</a>
    <center>
        <form action="search_student_action.jsp">
            <table border="1">
                <tr>
                    <td colspan="2" align="center">
                        Search Form
                    </td>
                </tr>
                <tr>
                    <td>
                        Name :
                    </td>   
                    <td>               
                        <input type="text" name="name" placeholder="first name"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        City :
                    </td>   
                    <td>               
                        <input type="text" name="city" placeholder="City"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Age :
                    </td>
                    <td>
                        <input type="text" name="age" placeholder="age"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type ="submit"/>
                    </td> 
                </tr>
            </table>
        </form>
    </center>
    <table border="1">
        <tr>
            <td colspan="2" align="center">
                A
            </td>
        </tr>
        <tr>
            <td>
                B
            </td>
            <td>C</td>
        </tr>    
        <tr>
            <td colspan="2" align="center">
                D
            </td>
        </tr>
        <tr>
            <td colspan="2">
                E
            </td>
        </tr>
    </table>
    <br>
    <table border="1">
        <tr>
            <td rowspan="2">
                A
            </td>
            <td>B</td>
        </tr>
        <tr>
            <td>C</td>
        </tr>
        <tr>
            <td>D</td>
            <td rowspan="2">F</td>
        </tr>
        <tr>
            <td>
                E
            </td>
        </tr>
    </table>

</body>
</html>
