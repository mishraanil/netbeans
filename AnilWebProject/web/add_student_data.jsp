<%-- 
    Document   : add_student_data
    Created on : Jan 6, 2017, 8:44:04 PM
    Author     : ICIT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="add_student_data_action.jsp">
            <table>
                <td>
                    Name:
                </td>
                <td>
                    <input type="text" name="name" placeholder="first name"/>
                </td>
                <tr>
                    <td>
                        Age:
                    </td>
                    <td>
                        <input type="text" name="age" placeholder="Age">
                    </td>
                </tr>
                
                <tr>
                    <td>
                        City:
                    </td>
                    <td>
                        <input type="text" name="city" placeholder="City">
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Add">
                    </td>
                    <td>
                        <input type="reset" value="Clear">
                    </td>
                </tr>
            </table>
                
                
        </form>
        
    </body>
</html>
