<%-- 
    Document   : circle_action
    Created on : Dec 29, 2016, 8:37:37 PM
    Author     : ICIT
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            int radius = Integer.parseInt(request.getParameter("radius"));
            
            String bill_date = request.getParameter("bdate");
            
            double area = 3.14 * radius * radius;
            double areaofcircumference = 2 * 3.14 * radius;
        %>
        <h1>
            Date is <%=bill_date%>
            Area of circle is <%=area%><br>
            Area of Circumference is <%=areaofcircumference%>
        </h1>
        <a href="circle_form.jsp">Back</a>
    </body>
</html>
