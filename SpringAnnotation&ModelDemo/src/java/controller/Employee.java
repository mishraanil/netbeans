/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;



@Controller
@RequestMapping("/employee")
public class Employee {
    @RequestMapping("/show_employee")
    public ModelAndView showEmpData(@RequestParam("name") String employee_name){
        ModelAndView view = new ModelAndView("empview");
        view.addObject("show_employee", employee_name);
        return view;
                
    }
}
    

